var pundit_analytics_app = angular.module("pundit_analytics", []);
pundit_analytics_app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol("[[");
    $interpolateProvider.endSymbol("]]");
});

// pundit_analytics_app.config(['$locationProvider', function($locationProvider) {
//     $locationProvider.html5Mode({
//         enabled: true,
//         requireBase: false
//     })
// }])
pundit_analytics_app.controller("signUpController", [
    "$scope",
    "$location",
    function($scope, $location) {
        console.log($location.search());
        $scope.user = {};
        $scope.passed_authentication = false;
        // if ($location.search().authenticated) {
        //     $scope.passed_authentication = true;

        // } else {
        //     $scope.passed_authentication = false;

        // }
        $scope.submit_new_user = function(user) {
            // $scope.master = angular.copy(user);
            console.log(user);
            console.log("NOW ADDING TO DATABASE");
            console.log($scope.user.uid);
            var db = firebase.firestore();
            $scope.user.created_at = new Date();
            $scope.user.targets = [];

            db.collection("users")
                .doc($scope.user.uid)
                .set($scope.user)
                .then(function(docRef) {
                    console.log("Document written with ID: ", docRef);
                    window.open("https://console.punditanalytics.com/", "_self");
                })
                .catch(function(error) {
                    console.error("Error adding document: ", error);
                });
        };

        // TODO: Replace the following with your app's Firebase project configuration
        var firebaseConfig = {
            apiKey: "AIzaSyDeQwLk0D6Xai9ATFlUhExQtMKDVGuW-hA",
            authDomain: "pundit-analytics.firebaseapp.com",
            databaseURL: "https://pundit-analytics.firebaseio.com",
            projectId: "pundit-analytics",
            storageBucket: "pundit-analytics.appspot.com",
            messagingSenderId: "389691317199",
            appId: "1:389691317199:web:83de7d3daf9421f7ced0ba",
            measurementId: "G-7XWN6D7ETT"
        };

        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                document.getElementById("loader").style.display = "none";
                console.log(user.toJSON());
                // var temp_dic = user.providerData[0];
                // console.log(user.uid);
                // var temp_uid = user.uid

                // delete temp_dic['uid']
                // temp_dic['uid'] = temp_uid;
                $scope.user = user.toJSON();
                document.getElementById("auth_message").style.display = "block";
                document.getElementById("almost_done").style.display = "block";

                document.getElementById("user_phone").innerText =
                    $scope.user.phoneNumber + ".";
                document.getElementById("user_form_id").style.display = "block";

                $scope.passed_authentication = true;
                // $scope.user.uid = user.uid;

                // ...
            } else {
                document.getElementById("auth_message").style.display = "none";
                document.getElementById("almost_done").style.display = "none";

                document.getElementById("user_phone").style.display = "none";
                document.getElementById("user_form_id").style.display = "none";
                // User is signed out.
                // ...
                var ui = new firebaseui.auth.AuthUI(firebase.auth());
                ui.start("#firebaseui-auth-container", {
                    signInFlow: "popup",
                    signInOptions: [
                        firebase.auth.PhoneAuthProvider.PROVIDER_ID

                    ],

                    callbacks: {
                        signInSuccessWithAuthResult: function(authResult, redirectUrl) {
                            // User successfully signed in.
                            // Return type determines whether we continue the redirect automatically
                            // or whether we leave that to developer to handle.
                            console.log("HEYY WAS SUCCESSFULL.");
                            document.getElementById("user_email").style.display = "block";

                            return false;
                        },
                        uiShown: function() {
                            // The widget is rendered.
                            // Hide the loader.
                            document.getElementById("loader").style.display = "none";
                        }
                    }
                    // Other config options...
                });
            }
        });

        $scope.sign_out = function() {
            firebase
                .auth()
                .signOut()
                .then(function() {
                    // Sign-out successful.
                    console.log("SIGNED OUT");
                    location.reload()
                })
                .catch(function(error) {
                    // An error happened.
                });
        };
    }
]);